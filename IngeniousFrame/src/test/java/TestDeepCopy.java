import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;

public class TestDeepCopy {

	public static void main(String[] args) {
		TurnBasedSquareBoard b = new TurnBasedSquareBoard(3, 0, 2);
		b.board[1]=9;
		b.printPretty();
		
		TurnBasedSquareBoard b2 = (TurnBasedSquareBoard) b.deepCopy();
		b2.printPretty();
		
		b.board[2] = 8;
		b2.board[5] = 7;
		b.printPretty();
		b2.printPretty();
	}

}
