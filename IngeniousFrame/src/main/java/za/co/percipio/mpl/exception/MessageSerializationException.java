package za.co.percipio.mpl.exception;

import za.co.percipio.mpl.Message;

public class MessageSerializationException extends RuntimeException{
    private static final long serialVersionUID = 4776159698059251966L;
    private Message message;
    public MessageSerializationException(Message message, Exception e) {
        super("Message object could not be serialized.", e);
        this.message = message;
    }

    public Message getMessageObject(){
        return message;
    }
}
