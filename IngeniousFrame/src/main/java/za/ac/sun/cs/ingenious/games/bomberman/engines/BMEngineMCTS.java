package za.ac.sun.cs.ingenious.games.bomberman.engines;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Constants;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.games.bomberman.BMEngine;
import za.ac.sun.cs.ingenious.games.bomberman.BMReferee;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.mcts.BMNode;
import za.ac.sun.cs.ingenious.games.bomberman.mcts.BMRandomPolicy;
import za.ac.sun.cs.ingenious.games.bomberman.mcts.BMUpdater;
import za.ac.sun.cs.ingenious.games.bomberman.mcts.MCTSRandomPlayer;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGenActionMessage;
import za.ac.sun.cs.ingenious.search.mcts.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.SingleValueUCTDescender;

/**
 * This engine uses Monte Carlo evaluation of a single ply search using random playouts.
 */
// TODO (see issue 154): Replace this using a regular single ply search, provided with a generic
// Monte Carlo evaluation function which takes a random playout policy.
// Also rename the class 
// TODO: Currently this player runs out of time because it does not respect the time controls - see issue 154.
public class BMEngineMCTS extends BMEngine {

    public static final int DEFAULT_NUM_SIMULATIONS = 3;
    private int numSimulations;
    
	public BMEngineMCTS(EngineToServerConnection toServer) {
        this(toServer, DEFAULT_NUM_SIMULATIONS);
	}

	public BMEngineMCTS(EngineToServerConnection toServer, int numSimulations) {
		super(toServer);
        this.numSimulations = numSimulations;
	}
	
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		BMGenActionMessage bmGenMove = (BMGenActionMessage) a; 
		return new PlayActionMessage(FlatMCTSGenMove(bmGenMove.getPlayerID(), bmGenMove.getRound()));
	}
	
	@Override
	public String engineName() {
		return "BMEngineMCTS";
	}
	
	public Action MCTSGenMove(int id, int round){
		BMNode node = new BMNode((BMBoard) board.deepCopy(), (short)id);
	    Action bestMove = MCTS.generateMove(node, new BMRandomPolicy(), new SingleValueUCTDescender(), new BMUpdater(), Constants.TURN_LENGTH, true);
	    return bestMove;
	}
	
	public Action FlatMCTSGenMove(int id, int round){
		board.printPretty();
		Log.info("Round: "+round+" player "+id);
		List<Action> actions = logic.generateActions(board, id);
		Action bestMove = null;
		long maxSurvival = Long.MIN_VALUE;
		long start = System.currentTimeMillis();
        for (Action action: actions) {
			long survivals = 0;
			for(int simulationNr = 0; simulationNr < numSimulations; simulationNr++) {
                int numPlayers = board.getPlayers().length;
				MCTSRandomPlayer[] MCTSRandomPlayers = new MCTSRandomPlayer[numPlayers];
				BMBoard newboard = (BMBoard) board.deepCopy();
				BMReferee controller = new BMReferee(null, MCTSRandomPlayers, newboard);
				for (int i = 0; i < numPlayers; i++){
					MCTSRandomPlayers[i] = new MCTSRandomPlayer(i, controller.getBoard());
					if (i == id) {
						MCTSRandomPlayers[i].setFirstMove(action);
					}
				}
				boolean firstTime = true;
				int simRoundNr = 0;
				while(!logic.isTerminal(controller.getBoard()) && simRoundNr < numSimulations){
					controller.playRound(new ArrayList<String>(),firstTime);
					firstTime = false;
					simRoundNr++;
				}
				survivals += newboard.score(id);
			}
			Log.info("	"+action.toString() + ": "+survivals);
			if(survivals > maxSurvival){
				bestMove = action;
				maxSurvival = survivals;
			}
		}
		long end = System.currentTimeMillis();
		return bestMove;
	}
}
