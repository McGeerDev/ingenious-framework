package za.ac.sun.cs.ingenious.games.loa;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;

import za.ac.sun.cs.ingenious.core.model.Coord;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;

public class LOABoard extends TurnBasedGameState {

	public static final int WHITE = 0;
	public static final int BLACK = 1;
	public static final int EMPTY = 2;

	private int boardSize;
	int numWhite;
	int numBlack;
	Deque<Coord> previousFrom;
	Deque<Coord> previousTo;
	Deque<Boolean> capture;
	private int[][] state;

	public LOABoard(int[][] state, int currentPlayer){
		super(currentPlayer,2);
		this.previousFrom  = new ArrayDeque<Coord>();
		this.previousTo  = new ArrayDeque<Coord>();
		this.capture = new ArrayDeque<Boolean>();
		this.boardSize = state.length;
		this.nextMovePlayerID = currentPlayer;
		int nw = 0;
		int nb = 0;
		this.state = new int[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j < boardSize; j++){
				if (state[i][j] == WHITE){
					nw++;
				} else if (state[i][j] == BLACK){
					nb++;
				}
				this.state[i][j] = state[i][j];

			}
		}
		this.numBlack = nb;
		this.numWhite = nw;
	}

	public int[][] getState(){
		return this.state;
	}

	public int getBoardSize() {
		return boardSize;
	}

	public String toString(){
		String s = "";
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j < boardSize - 1; j++){
				switch (state[i][j]) {
				case WHITE:
					s = s + "W ";
					break;
				case BLACK:
					s = s + "B ";
					break;
				case EMPTY:
					s = s + ". ";
					break;
				}
			}
			switch (state[i][boardSize - 1]) {
			case WHITE:
				s = s + "W\n";
				break;
			case BLACK:
				s = s + "B\n";
				break;
			case EMPTY:
				s = s + ".\n";
				break;
			}
		}
		return s;
	}

	@Override
	public void printPretty() {
		System.out.println(this);
	}

}
