package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;

public class STTTGameState extends TurnBasedGameState {
	
	public TurnBasedSquareBoard[] playerBoards;
	public List<List<Move>> moveHistories;

	public STTTGameState() {
		super(0,2);
		playerBoards = new TurnBasedSquareBoard[2];
		playerBoards[0] = new TurnBasedSquareBoard(3, 0,2);
		playerBoards[1] = new TurnBasedSquareBoard(3, 0,2);
		moveHistories = new ArrayList<List<Move>>();
		moveHistories.add(new ArrayList<Move>());
		moveHistories.add(new ArrayList<Move>());
	}

	@Override
	public void printPretty() {
		Log.info("Player 0's board:");
		playerBoards[0].printPretty();
		Log.info("Player 1's board:");
		playerBoards[1].printPretty();
	}

}
