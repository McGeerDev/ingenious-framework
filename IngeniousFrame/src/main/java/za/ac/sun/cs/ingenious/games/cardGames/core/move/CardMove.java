package za.ac.sun.cs.ingenious.games.cardGames.core.move;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;

/**
 * The Class CardMove.
 *
 * @param <Location> the generic type
 */
public abstract class CardMove<Location extends Enum<Location>> implements Action{
	
	/** The Constant serialVersionUID. */
	protected static final long serialVersionUID = 1L;
	
	/** player needed to verify if it's player's turn. */
	protected int player;
	
	/** The card to move. */
	protected Card card;
	
	/** The old location. */
	protected Location oldLoc;
	
	/** The new location. */
	protected Location newLoc;
	
	/**
	 * Instantiates a new card move.
	 *
	 * @param player the player
	 * @param card the card
	 * @param oldLoc the old loc
	 * @param newLoc the new loc
	 */
	public CardMove(int player, Card card, Location oldLoc, Location newLoc){
		this.card = card;
		this.oldLoc = oldLoc;
		this.newLoc = newLoc;
		this.player = player;
	}
	
	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	public int getPlayer() {
		return player;
	}
	
	/**
	 * Gets the card.
	 *
	 * @return the card
	 */
	public Card getCard() {
		return card;
	}
	
	/**
	 * Gets the old loc.
	 *
	 * @return the old loc
	 */
	public Location getOldLoc() {
		return oldLoc;
	}
	
	/**
	 * Gets the new loc.
	 *
	 * @return the new loc
	 */
	public Location getNewLoc() {
		return newLoc;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other){
		if(other == null){return false;}
		if(other == this){return true;}
		if(!(other instanceof CardMove)){return false;}
		
		CardMove<Location> otherPCM = (CardMove<Location>) other;
		if(this.card.equals(otherPCM.getCard()) && this.player == otherPCM.getPlayer() && 
				this.oldLoc.equals(otherPCM.getOldLoc()) && this.newLoc.equals(otherPCM.getNewLoc())){
			return true;
		}
		return false;	
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return ("Move " + this.card.toString() + " from " + this.oldLoc + " to " + this.newLoc + ".");
	}
}
