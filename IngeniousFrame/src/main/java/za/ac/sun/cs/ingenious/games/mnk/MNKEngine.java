package za.ac.sun.cs.ingenious.games.mnk;

import com.esotericsoftware.minlog.Log;

import java.util.List;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.Constants;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.search.mcts.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.UCTDescender;


public class MNKEngine extends Engine{
	
	private MNKBoard board;
	private MNKLogic logic;
	private MNKFinalEvaluator eval;
	
	public MNKEngine(EngineToServerConnection toServer){
		super(toServer);
		board = null;
		logic = new MNKLogic();
		eval = new MNKFinalEvaluator();
	}
	
	//
	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		this.board = new MNKBoard(((MNKInitGameMessage)a).getHeight(),((MNKInitGameMessage)a).getWidth(),((MNKInitGameMessage)a).getK(),0);
	}
	
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		//return getRandomMove();
		///**
		MNKAction move = genMCTSMove();
		return new PlayActionMessage(move);
		//*/
	}
	
	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a) {
		logic.makeMove(board, (MNKAction) a.getMove());
	}
	
	@Override
	public String engineName() {
		return "MNKEngine";
	}
	
	//AI - MCTS
	
	public MNKAction genMCTSMove(){
		MCTSNode<MNKBoard> root = new MCTSNode<MNKBoard>(board, logic, null, null);
		return (MNKAction)MCTS.generateMove(root, new RandomPolicy<MNKBoard, MCTSNode<MNKBoard>>(logic,eval, false),
				new UCTDescender<MCTSNode<MNKBoard>>(),
				new SimpleUpdater<MCTSNode<MNKBoard>>(),
				Constants.TURN_LENGTH, true);
	}	
	
	public PlayActionMessage getRandomMove(){
		List<Action> al = logic.generateActions(board, playerID);
		if(al.isEmpty()){
			return new PlayActionMessage(null);
		}
		Random rand = new Random();
		int r = rand.nextInt(al.size());
		String s = "rand: "+r;
		Log.info(s);
		return new PlayActionMessage(al.get(r));
	}

	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
		// TODO Auto-generated method stub
		
	}
}
