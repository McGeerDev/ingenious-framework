package za.ac.sun.cs.ingenious.core.model;

import com.rits.cloning.Cloner;

import java.util.Observable;

/**
 * Represents one state of some game. Must be extended with specific information for each
 * game. Note that for both perfect and imperfect information games, the GameState
 * contains <b>all</b> information that constitutes a state of the game. In games of
 * imperfect information, a GameState object may represent an information set that a
 * player of the game sees. But it must also support representing the complete state of
 * the game as seen e.g. by the referee.
 * 
 * @author Michael Krause
 */
public abstract class GameState extends Observable {

	protected static final Cloner cloner=new Cloner();

	protected int numPlayers;

	public GameState(int numPlayers) {
		this.numPlayers = numPlayers;
	}
	
	public int getNumPlayers() {
		return numPlayers;
	}
	
	/**
	 * Print a representation of this state. This is used e.g. to display the 
	 * GameState after a player made their move.
	 */
    // TODO - should take a logging level as a parameter - see issue 147
	public abstract void printPretty();
	
	/**
	 * This can provide a deep copy of any GameState using the Cloning library. 
	 * Can be overridden to provide faster manual copying.
	 * @return A deep copy of this GameState
	 */
	public GameState deepCopy() {
		return cloner.deepClone(this);
	}
	
}
