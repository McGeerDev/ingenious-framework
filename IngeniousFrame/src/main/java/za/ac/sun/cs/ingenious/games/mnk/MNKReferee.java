package za.ac.sun.cs.ingenious.games.mnk;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.PerfectInformationAlternatingPlayReferee;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
	
public class MNKReferee extends PerfectInformationAlternatingPlayReferee<MNKBoard,MNKLogic,MNKFinalEvaluator> {
	
	//Constructor
	public MNKReferee(MatchSetting match, PlayerRepresentation[] players){
		super(match,players,new MNKBoard(match.mnk_getH(),match.mnk_getW(),match.mnk_getK(),0),new MNKLogic(), new MNKFinalEvaluator());
	}
	
	//Just printing the core game settings of MNK
	@Override
	protected void beforeGameStarts(){
		StringBuilder s = new StringBuilder();
		s.append("\nGame Settings:\n");
		s.append("Height: " + this.currentState.getHeight() + "\n");
		s.append("Width: " + this.currentState.getWidth() + "\n");
		s.append("K: " + this.currentState.getK() + "\n");
		
		Log.info(s);
	}
	
	@Override
	protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
		return new MNKInitGameMessage(currentState.getHeight(), currentState.getWidth(), currentState.getK());
	}
}
