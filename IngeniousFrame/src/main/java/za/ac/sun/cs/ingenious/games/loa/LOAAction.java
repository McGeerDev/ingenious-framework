package za.ac.sun.cs.ingenious.games.loa;

import za.ac.sun.cs.ingenious.core.model.Coord;
import za.ac.sun.cs.ingenious.core.model.Action;

public class LOAAction implements Action {

	private static final long serialVersionUID = 1L;
	
	
	private Coord from;
	private Coord to;
	
	
	
	public LOAAction(Coord from, Coord to){
		this.from = from;
		this.to = to;
	}
	
	public String[] toArray(){
		return new String[] {from.getX()+"", from.getY()+"", to.getX()+"", to.getY()+""};
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LOAAction other = (LOAAction) obj;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}

	public Coord getFrom() {
		return from;
	}

	public void setFrom(Coord from) {
		this.from = from;
	}

	public Coord getTo() {
		return to;
	}

	public void setTo(Coord to) {
		this.to = to;
	}
	
	public String toString(){
		return from.toString() + " -> " + to.toString();
	}
}
