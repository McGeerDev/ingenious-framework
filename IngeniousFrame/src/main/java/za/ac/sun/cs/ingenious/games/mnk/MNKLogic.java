package za.ac.sun.cs.ingenious.games.mnk;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;

public class MNKLogic implements TurnBasedGameLogic<MNKBoard> {

	@Override
	public boolean validMove(MNKBoard fromState, Move move) {
		return fromState.getBoard()[((MNKAction)move).getH()][((MNKAction)move).getW()] == 0;
	}

	@Override
	public boolean makeMove(MNKBoard fromState, Move move) {
		int h = ((MNKAction) move).getH();
		int w = ((MNKAction) move).getW();
		fromState.getBoard()[h][w] = ((MNKAction) move).getPlayer()+1;
		nextTurn(fromState, move);
		return validMove(fromState, move);
	}

	@Override
	public void undoMove(MNKBoard fromState, Move move) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Action> generateActions(MNKBoard fromState, int forPlayerID) {
		ArrayList<Action> al = new ArrayList<>();
		
		for(short h=0; h<fromState.getHeight();h++){
			for(short w=0; w<fromState.getWidth();w++){
				if(fromState.getBoard()[h][w]==0){
					al.add(new MNKAction(h,w, fromState.nextMovePlayerID));
				}
			}
		}
		return al;
	}

	@Override
	public boolean isTerminal(MNKBoard state) {
		//Rows
				for(int row=0;row<state.getHeight();row++){
					int last = 0;
					int in_A_Row = 0;
					for(int col=0;col<state.getWidth();col++){
						int curr = state.getBoard()[row][col];
						
						if(curr!=0 && curr==last){
							in_A_Row++;
							if(in_A_Row==state.getK()-1){
								state.setWinner((short)(curr-1));
								return true;
							}
						}
						else{
							in_A_Row = 0;
						}
						last = curr;
					}
				}
				
				//Collums
				for(int col=0;col<state.getWidth();col++){
					int last = 0;
					int in_A_Row = 0;
					for(int row=0;row<state.getHeight();row++){
						int curr = state.getBoard()[row][col];
						
						if(curr!=0 && curr==last){
							in_A_Row++;
							if(in_A_Row==state.getK()-1){
								state.setWinner((short)(curr-1));
								return true;
							}
						}
						else{
							in_A_Row = 0;
						}
						last = curr;
					}
				}
				
				//Diagonals (right)
				//For each field
				for(int row=0;row<state.getHeight();row++){
					for(int col=0;col<state.getWidth();col++){
						//Try to descend
						int last = 0;
						int in_A_Row = 0;
						for(int r = row, c=col;r<state.getHeight() && c<state.getWidth();r++, c++){
							int curr = state.getBoard()[r][c];
							if(curr!=0 && curr==last){
								in_A_Row++;
								if(in_A_Row==state.getK()-1){
									state.setWinner((short)(curr-1));
									return true;	
								}
							}
							else{
								in_A_Row = 0;
							}
							last = curr;
						}
					}
				}
				
				//Diagonals (left)
						//For each field
						for(int row=0;row<state.getHeight();row++){
							for(int col=0;col<state.getWidth();col++){
								//Try to descend
								int last = 0;
								int in_A_Row = 0;
								for(int r = row, c=col;r<state.getHeight() && r>=0 && c<state.getWidth() && c>=0;r++, c--){
									int curr = state.getBoard()[r][c];
									if(curr!=0 && curr==last){
										in_A_Row++;
										if(in_A_Row==state.getK()-1){
											state.setWinner((short)(curr-1));
											return true;	
										}
									}
									else{
										in_A_Row = 0;
									}
									last = curr;
								}
							}
						}
				
				return full(state);
	}

	
	public boolean full(MNKBoard state){
		for(int i=0;i<state.getHeight();i++){
			for(int j=0;j<state.getWidth();j++){
				if(state.getBoard()[i][j] == 0){
					return false;
				}
			}
		}
		return true;
	}
}
