package za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;

//Values for symbols don't play a role in Uno.
public enum UnoSymbols implements CardFeature{
	ZERO(0),
	ONE(1),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9);

	int value;
	
	UnoSymbols(int value){
		this.value = value;
	}
	
	@Override
	public int getValue() {
		return this.value;
	}

	@Override
	public void setValue(int value) {
		this.value = value;
	}
	
//	public static Iterator<UnoSymbols> iterator() {
//		ArrayList<UnoSymbols> al = new ArrayList<>(Arrays.asList(UnoSymbols.values()));
//		return al.iterator();
//	}
	
}
