# CardGameState

#### A card game state is mainly represented by a **TreeMap**...

...with with **Card as the key** and **ArrayList of locations** as according value.

Generic types are the two features for the card and also an **enum** with all possible locations of a game in it. 
(e. g. players or roles, draw piles, discard piles, ect...).

It get's a deck as an argument for the constructor, also the initial location for all cards (e. g. draw pile) and the ID of the first player.

**Please note:** in a card game state a Card<?,?> is no longer a proper representation for an actual card. It is only  a key for the map.
Now the number of locations in the ArrayList indicate how many cards of a type are in the game and even more importantly, where.
If the ArrayList of locations for a specific card type is empty this card doesn't exist in the game. 
By adding a new type Card<?,?> this class will add a new location in the ArrayList of the key *Card<?,?>* (or create a new entry with this new card type).

**Also important:** as long as *roles* are **not** introduced to this framework consider 
**list all player locations first and in a row!** in the **location enum**.
This saves time and effort when implementing the logic of a game. You can easily access a specific location by '<YourEnum>.values()[indexOfLocation]'. 
So player **0** would correspond to <YourEnum>.values()[**0**].
This might change when *roles* are introduced.