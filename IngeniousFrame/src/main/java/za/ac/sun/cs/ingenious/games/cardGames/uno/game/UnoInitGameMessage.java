package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

public class UnoInitGameMessage extends InitGameMessage{

	private static final long serialVersionUID = 1L;
	private CardRack<UnoSymbols, UnoSuits> rack;
	private int playerID;
	private int numPlayers;
	
	public UnoInitGameMessage(CardRack<UnoSymbols, UnoSuits> rack, int playerID, int numPlayers){
		this.rack = rack;
		this.playerID = playerID;
		this.numPlayers = numPlayers;
	}
	
	public CardRack<UnoSymbols, UnoSuits> getRack(){
		return rack;
	}
	
	public int getNumPlayers(){
		return numPlayers;
	}
	
	public int getPlayerID(){
		return playerID;
	}
}
