package za.ac.sun.cs.ingenious.games.domineering;

import za.ac.sun.cs.ingenious.core.Constants;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.search.mcts.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.UCTDescender;

public class DomineeringMCTSEngine extends DomineeringEngine {
	
	public DomineeringMCTSEngine(EngineToServerConnection toServer) {
		super(toServer);
	}
	
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		if (board == null) {
			throw new RuntimeException(engineName() + ": board was null when GenMoveMessage was received! (Perhaps no InitGameMessage was sent?)");
		}
		// This engine chooses a move with MCTS
		MCTSNode<TurnBasedSquareBoard> root = new MCTSNode<TurnBasedSquareBoard>(board, logic, null, null);
		return new PlayActionMessage(MCTS.generateMove(root,
				new RandomPolicy<TurnBasedSquareBoard,MCTSNode<TurnBasedSquareBoard>>(logic, new DomineeringFinalEvaluator(), false),
				new UCTDescender<MCTSNode<TurnBasedSquareBoard>>(),
				new SimpleUpdater<MCTSNode<TurnBasedSquareBoard>>(),
				Constants.TURN_LENGTH, true));	
	}
	
	@Override
	public String engineName() {
		return "DomineeringMCTSEngine";		
	}
	
}
