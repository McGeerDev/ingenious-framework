package za.ac.sun.cs.ingenious.search.ismcts;

import static java.lang.Math.sqrt;

import com.esotericsoftware.minlog.Log;

import java.util.List;

import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.mcts.SearchNode;
import za.ac.sun.cs.ingenious.search.mcts.SimpleDescender;
import za.ac.sun.cs.ingenious.search.mcts.TreeDescender;

public class SOISMCTSUCTDescender<S extends TurnBasedGameState, N extends DeterminizedSearchNode<S> & SearchNode<S,N>> extends SimpleDescender<N> {
	
	public SOISMCTSUCTDescender(GameLogic<S> logic) {
		this.logic = logic;
	}

	GameLogic<S> logic;
	
	@Override
	protected N bestChild(N node, int totalNofPlayouts) {
		// Like the implementation in SimpleDescender, this returns the child with the highest
		// search value for the given node. However, only nodes that are available for the current
		// determiniziation are considered.
		// Furthermore, the determinization is applied to the next node before the search continues
		List<N> children = node.getExpandedChildren();
		if(children.isEmpty()){			
			return null;
		}

		double highestValue = Double.NEGATIVE_INFINITY;
		N bestMove = null;
		for(N child : children){
			if (!logic.validMove(node.getDeterminizedState(), child.getMove()))
				continue;
			double childValue = this.searchValue(child, node.getCurrentPlayer(), totalNofPlayouts);
			if( childValue > highestValue ){
				bestMove = child;
				highestValue = childValue; 
			}
		}
		if (bestMove != null) {
			bestMove.setParentDeterminizedState((S) node.getDeterminizedState().deepCopy());
		}
		return bestMove;	
	}
	
	@Override
	public double searchValue(N node, int forPlayerID, int totalNofPlayouts) {
		double parentPlayouts = node.getParent().getVisitsPerChildNodes().get(node.getMove()).intValue();	
		
		double barX = node.getReward()[forPlayerID]/(double)node.getVisits();
		//double C = 1.0/((double)totalNofPlayouts/50.0);
		
		double uncertainty  = 2.0*(1.0/sqrt(2.0))*sqrt(2.0*Math.log(parentPlayouts)/(double)node.getVisits());
		
		return barX+uncertainty;
	}

}
