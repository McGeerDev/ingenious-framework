package za.ac.sun.cs.ingenious.games.ingenious;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.Variant;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IngeniousScoreKeeper;
import za.ac.sun.cs.ingenious.games.ingenious.engines.Score;

public class IngeniousController implements Runnable {

	public final int NUMBER_OF_TILES;
	public final int NUMBER_OF_COLOURS;
	public final int NUMBER_OF_PLAYERS;

	private final ServerSocket serverSocket;
	private final int port;
	private final MatchSetting matchSettings;
	volatile int numberOfEngines = 0;
	private IngeniousEngineConnection engineConns[];
	private volatile boolean gameLoop = true;
	private ArrayList<IngeniousRack> playerRacks;
	private BagSequence gameBag;
	private IngeniousBoard gameBoard;
	private IngeniousScoreKeeper scoreKeeper;

	/*
	 * Engines and variants are paired by key
	 */
	private String[] engineNames;
	private Variant[] variants;

	public IngeniousController(MatchSetting match) throws Exception {
		this.matchSettings = match;
		this.engineNames = match.getEngineNames();
		this.NUMBER_OF_TILES = match.getBoardSize();
		this.NUMBER_OF_COLOURS = match.getNumColours();
		this.NUMBER_OF_PLAYERS = match.getNumPlayers();
		this.variants = match.getVariants();
		this.gameBoard = new IngeniousBoard(match.getBoardSize(),NUMBER_OF_COLOURS);
		this.scoreKeeper = new IngeniousScoreKeeper(NUMBER_OF_PLAYERS, NUMBER_OF_COLOURS);

		this.engineConns = new IngeniousEngineConnection[matchSettings.getNumPlayers()];
		this.playerRacks = new ArrayList<IngeniousRack>(NUMBER_OF_PLAYERS);
		this.gameBag = new DefaultBag(120, NUMBER_OF_COLOURS);

		try {
			serverSocket = new ServerSocket(0);
			this.port = serverSocket.getLocalPort();
			System.out.println("Controller created on port" + this.port);

		} catch (IOException e) {
			throw new Exception("ServerSocket not created: Controller failed.");
		}
	}

	public int getPort() {
		return this.port;
	}
	
	public MatchSetting getMatchSetting(){
		return this.matchSettings;
	}

	public synchronized void addEngine(IngeniousEngineConnection engineConn, int position) {
		if (engineConns[position] == null) {
			engineConns[position] = engineConn;
			numberOfEngines++;
			if (numberOfEngines == matchSettings.getNumPlayers()) {
				try {
					this.serverSocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("The number of  engines has just been increased to : " + numberOfEngines);
		}
		for (int i = 0; i < engineConns.length; i++) {
			System.out.println(engineConns[i]);
		}
	}

	private void acceptJoiningPlayers() {
		while (numberOfEngines < matchSettings.getNumPlayers()) {
			try {
				Socket client = this.serverSocket.accept();

				IngeniousEngineConnection engine = new IngeniousEngineConnection(this, client);
			} catch (IOException e) {
				System.out.println("failed to connect new client");
			}
			System.out.println("At end of while loop: " + numberOfEngines);
		}
	}

	private void initGameRacks() {
		for (int i = 0; i < engineConns.length; i++) {
			playerRacks.add(i, getRack(variants[i].getRackSize()));
		}

		distributeRacks();
	}

	private IngeniousRack getRack(int rackSize) {
		if (rackSize == 0) {
			return null;
		}
		IngeniousRack rack = new IngeniousRack(rackSize);
		for (int i = 0; i < rackSize; i++) {
			rack.add(gameBag.draw());
		}

		return rack;
	}

	private void distributeRacks() {
		for (IngeniousEngineConnection engine : engineConns) {
			for (int i = 0; i < this.NUMBER_OF_PLAYERS; i++) {
				if (variants[i].isOpponentsRackSizeVisible() || i == engine.id) {
					engine.setRack(playerRacks.get(i), i);
				}
			}
		}
	}

	private void distributeBags() {
		for (IngeniousEngineConnection engine : engineConns) {
				engine.setBag(gameBag.viewVisible(gameBag.size()));
		}
	}

	private void distributeDraw(Tile drawnTile, int playerId) {
		for (IngeniousEngineConnection engine : engineConns) {
			for (int i = 0; i < this.NUMBER_OF_PLAYERS; i++) {
				if (variants[i].isOpponentsRackSizeVisible() || i == playerId) {
					engine.draw(drawnTile, playerId);
				}
			}
		}
	}

	private void distributeAcceptedMove(int playerId, IngeniousAction move) {
		for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
			if (i != playerId) {
				engineConns[i].playMove(move);
			}
		}
	}

	public void run() {
		acceptJoiningPlayers();

		System.out.println("CONTROLLER CONNECTION CONCLUDED");

		initGameRacks();
		distributeBags();

		System.out.println("CONTROLLER SETUP CONCLUDED");

		while (gameLoop) {
			for (IngeniousEngineConnection engineTurn : engineConns) {
				int playerId = engineTurn.id;
				System.out.println("CURRENT PLAYER NUMBER : " + playerId);

				IngeniousAction generatedMove = engineTurn.genMove();

				if (generatedMove == null) {
					System.out.println("generate move pass.");
					break;
				}

				if (this.gameBoard.makeMove(generatedMove)) {
					distributeAcceptedMove(playerId, generatedMove);
					System.out.println(gameBoard);
				} else {
					System.out.println(gameBoard.validMove(generatedMove));
					System.out.println("Invalid move");
					break;
				}

				System.out.println(generatedMove.getTile()
						+ " tile is removed from rack");

				playerRacks.get(playerId).remove(generatedMove.getTile());

				int[] result = scoreKeeper.updateScore(playerId, gameBoard);

				System.out.println(gameBoard);

				Score playerScore = scoreKeeper.getScore(playerId);

				if (playerScore.outrightWin(variants[playerId]
						.getMaximumColourScore())) {
					System.out.println("GAME WON BY PLAYER "
							+ engineNames[playerId]);
					for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
						System.out.println(scoreKeeper.getScore(i));
					}
					gameLoop = false;
					break;
				} else if (gameBoard.full()) {
					System.out.println("GAME WON BY PLAYER "
							+ scoreKeeper.getLeader());
					for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
						for (int j = 0; j < NUMBER_OF_COLOURS; j++) {
							System.out.print(scoreKeeper.getScore(i)
									.getColourScore(j) + " ");
						}
						System.out.println();
					}
					gameLoop = false;
					break;
				}
				Tile drawnTile = gameBag.draw();
				distributeDraw(drawnTile, playerId);
				playerRacks.get(playerId).add(drawnTile);

				System.out.println("Player Leading at end of round : "
						+ scoreKeeper.getLeader());
			}
		}
	}
}
