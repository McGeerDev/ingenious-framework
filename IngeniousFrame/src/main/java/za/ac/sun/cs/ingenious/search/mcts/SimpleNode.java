package za.ac.sun.cs.ingenious.search.mcts;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;

/**
 * A general implementation of the SearchNode interface, suitable for many games
 *
 * @param <S> The type of GameState to operate on
 */
public abstract class SimpleNode<S extends GameState, N extends SimpleNode<S,N>> implements SearchNode<S,N> {

	protected N parent;
	protected Action parentToThisMove;
	
	/**
	 * The searchnodes that have already been extended.
	 */
	protected List<N> expandedChildren;
	
	/**
	 * The possible moves, that have not yet been tried.
	 */
	protected List<Action> unExpandedMoves;
	
	/**
	 * Number of visits to this node.
	 */
	protected int visits = 0;
	
	/**
	 * Number of wins/points for this node.
	 */
	protected double[] rewards;
	
	/**
	 * Depth of this node in the logginsearch tree.
	 */
	protected int depth;
	
	protected S currentState;
	protected GameLogic<S> logic;
	
	/**
	 * Creates a new simple search tree note with basic functionality.
	 * The move that leads to this node will be executed in the constructor.
	 * @param parentState The GameState in the previous node. The move that leads to this node must not have been made! A copy of this will be made in the constructor.
	 * @param logic Object containing the game logic.
	 * @param toThisNode The move that leads to this node.
	 * @param parent The parent node, null if this node is root.
	 */
	public SimpleNode(S parentState, GameLogic<S> logic, Action toThisNode, N parent) {
		this.logic = logic;
		this.parent = parent;
		if (parent != null) {
			this.depth = parent.getDepth()+1;
		} else {
			this.depth = 0;
		}
		this.currentState = (S) parentState.deepCopy();
		this.parentToThisMove = toThisNode;
		if(toThisNode != null)
			logic.makeMove(this.currentState, toThisNode);
		if (logic.getCurrentPlayersToAct(parentState).size()!=1) {
			String errorMsg = "SimpleNode only works with GameStates where one player moves at a time";
			Log.error("search.mcts.SimpleNode", errorMsg);
			throw new RuntimeException(errorMsg);
		}
		expandedChildren = new ArrayList<N>();
		unExpandedMoves = logic.generateActions(this.currentState,getCurrentPlayer());
		rewards = new double[parentState.getNumPlayers()];
	}
	
	@Override
	public List<N> getExpandedChildren() {
		return expandedChildren;
	}
	
	@Override
	public Action getMove() {
		return parentToThisMove;
	}
	
	@Override
	public int getCurrentPlayer() {
		return logic.getCurrentPlayersToAct(currentState).iterator().next();
	}
	
	@Override
	public S getGameState() {
		return currentState;
	}
	
	@Override
	public int getVisits() {
		return visits;
	}
	
	@Override
	public double[] getReward() {
		return rewards;
	}

	@Override
	public N getParent() {
		return parent;
	}

	@Override
	public void addPlayout(double[] d) {
		visits++;
		if (d.length != rewards.length)
			throw new RuntimeException("Cannot add playout with " + d.length + " players; expected " + rewards.length + " players.");
		for (int i = 0; i < rewards.length; i++) {
			rewards[i] += d[i];
		}
	}
	
	@Override
	public int getDepth() {
		return depth;
	}

	@Override
	public boolean expandable() {
		return getMovesToExpand().size() > 0 && (!logic.isTerminal(currentState));
	}

	@Override
	public String toString() {
		if(parentToThisMove != null){
			return "depth :" +depth + " player: "+ getCurrentPlayer() +  " " + parentToThisMove.toString() +"	" + "("+rewards+"	|"+visits+")";
		}else{
			return "depth :" +depth + " player: "+ getCurrentPlayer() +  " "  + "("+rewards+"	|"+visits+")";
		}
	}

	@Override
	public List<Action> getMovesToExpand() {
		return unExpandedMoves;
	}
}
