package za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves;

import za.ac.sun.cs.ingenious.core.model.Action;

public class IdleAction implements Action {

	private static final long serialVersionUID = 1L;

	public String toString(){
		return "idle";
	}
}
