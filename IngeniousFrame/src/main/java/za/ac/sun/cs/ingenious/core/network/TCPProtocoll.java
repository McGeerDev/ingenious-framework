package za.ac.sun.cs.ingenious.core.network;

public class TCPProtocoll {
	public static final int PORT = 61234;
	public static final String SERVER = "localhost";
	
	
	//Lobby Connection
	public static final String NEWMATCH = "NewMatch";
	public static final String REFRESH = "Refresh";
	public static final String JOIN = "JoinLobby";
	public static final String MATCHSETTING = "MatchSetting";
	public static final String SUCCESS = "Success";
	public static final String FAILURE = "Failure";
	
	//EngineConfig Connection
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String GENMOVE = "genmove";
	public static final String PLAYMOVE = "playmove";
	
}
