package za.ac.sun.cs.ingenious.games.cardGames.core.move;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;

public class DrawCardMove<Location extends Enum<Location>> extends CardMove<Location>{

	private static final long serialVersionUID = 1L;

	public DrawCardMove(int player, Card card, Location oldLoc, Location newLoc) {
		super(player, card, oldLoc, newLoc);
	}

	@Override
	public String toString(){
		return ("Draw a card from draw pile: " + super.card.toString());
	}
	
}
