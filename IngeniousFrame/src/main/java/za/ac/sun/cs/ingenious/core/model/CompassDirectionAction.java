package za.ac.sun.cs.ingenious.core.model;

/**
 * This class is used for Actions that represent movement by a player in a
 * specific {@link CompassDirection}.
 */
public class CompassDirectionAction implements Action {

	private static final long serialVersionUID = 1L;

	private int playerID;
	private CompassDirection dir;
	
	public CompassDirectionAction(int playerID, CompassDirection dir) {
		this.playerID = playerID;
		this.dir = dir;
	}
	
	public CompassDirection getDir() {
		return dir;
	}

	public int getPlayerID() {
		return playerID;
	}
	
	@Override
	public String toString(){
		return "Player " + playerID + " CompassDirectionAction, dir " + dir;
	}
}

