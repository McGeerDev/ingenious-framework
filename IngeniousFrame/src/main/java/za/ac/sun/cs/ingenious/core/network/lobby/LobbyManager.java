package za.ac.sun.cs.ingenious.core.network.lobby;

import com.esotericsoftware.minlog.Log;

import java.util.HashMap;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;

/**
 * Class to hold and manage all open lobbies.
 * @author Stephan Tietz
 *
 */
public class LobbyManager {
	private volatile HashMap<String, LobbyHost> openLobbies;
	private volatile HashMap<String, ClientHandler> managers;
	
	/**
	 * Creates a new lobby manager without any open lobies.
	 */
	public LobbyManager(){
		openLobbies = new HashMap<String, LobbyHost>();
		managers  = new HashMap<String, ClientHandler>();
	}
	
	/**
	 * @param name of the lobby for which settings are requested. 
	 * @return The match settings of the specified lobby.
	 */
	public MatchSetting getMatchSettings(String name){
		return openLobbies.get(name).getMatchSetting();
	}
	
	
	/**
	 * @param lobbyName
	 * @return A LobbyHost containing the referee for the specified lobby.
	 */
	public LobbyHost getReferee(String lobbyName){
		return openLobbies.get(lobbyName);
	}
	
	
	/**
	 * @return The names of the started referees.
	 */
	public synchronized String[] getRefereeNames(){
		
		String[] refereeNames = openLobbies.keySet().toArray(new String[openLobbies.size()]);
		return refereeNames;
	}
	
	
	/**
	 * Add a new lobby.
	 * @param lobbyName name of the lobby. Must be unique.
	 * @param lobbyHoster
	 */
	public synchronized boolean addLobby(String lobbyName, LobbyHost lobbyHoster) {
		if(!openLobbies.containsKey(lobbyName)){
			openLobbies.put(lobbyName, lobbyHoster);
			return true;
		}else{
			Log.error("Lobbyname already taken.");
			return false;
		}
	}
	
	public boolean isLobbyNameAvailable(String name){
		return !openLobbies.containsKey(name);
	}
	
	/**
	 * Return a list of names for all managers currently connected
	 * to this GameServer
	 * @return String[]
	 */
	public synchronized String[] getPlayerNames(){
		String[] playerNames = managers.keySet().toArray(new String[managers.size()]);
		
		return playerNames;
	}
	
	

	/**
	 * Store a connection to a manager in the managers HashMap
	 * 
	 * @param playerName
	 * @param manager
	 */
	public synchronized void addManager(String playerName,
			ClientHandler manager) {
		managers.put(playerName, manager);
	}

	/**
	 * remove a previously stored manager
	 * @param playerName
	 */
	public synchronized void removeManager(String playerName) {
		Log.info("Removed player name " + playerName);
		managers.remove(playerName);
	}
	
	/**
	 * check the managers hashmap for a previous connection
	 * using the same name.
	 * @param playerName
	 * @return boolean
	 */
	public synchronized boolean checkDuplicateManagerName(String playerName) {
		return managers.containsKey(playerName);
	}


	/**
	 * Removes a lobby from the list of open lobbies.
	 * @param name the lobby to be removed from the open lobbies list.
	 */
	public void unregisterLobby(String name) {
		Log.info("Removed lobby with name " + name);
		openLobbies.remove(name);
	}
}
