package za.ac.sun.cs.ingenious.search.minimax;

import java.util.List;

import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;

/**
 * This implements a minimax search procedure with alpha-beta pruning for 2-player games
 * TODO drop restriction to 2-player games
 * @author Michael Krause
 * @author steven
 */
public class MiniMax<S extends TurnBasedGameState, L extends GameLogic<S>, E extends GameEvaluator<S>> {

	private int maxSearchDepth;
	private E eval;
	private int currentPlayerID;
	private L logic;
	
	public MiniMax(int maxSearchDepth, E eval, L logic, int playerID) {
		this.maxSearchDepth = maxSearchDepth;
		this.eval = eval;
		this.logic = logic;
		this.currentPlayerID = playerID;
	}
	
	public Action generateMove (S start) {
		List<Action> actions = orderMoves(logic.generateActions(start,start.nextMovePlayerID), start);
		Action bestMove = actions.get(0);
		double bestValue = Double.NEGATIVE_INFINITY;
		double alpha = Double.NEGATIVE_INFINITY;
		double beta = Double.POSITIVE_INFINITY;
		
		for (Action child : actions) {
			S clonedState = (S) start.deepCopy();
			logic.makeMove(clonedState, child);
			double value = MinMove(clonedState, alpha, beta, 0);			

			if (bestValue < value) {
				bestValue = value;
				bestMove = child;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(alpha >= beta){
				break;
			}
		}

		return bestMove;
	}
	
	private double MaxMove(S state,double alpha,double beta, int depth) {

		if (logic.isTerminal(state) || this.maxSearchDepth == depth) {
			return eval.getScore(state)[currentPlayerID];
		}

		double bestValue = Double.NEGATIVE_INFINITY;

		List<Action> actions = orderMoves(logic.generateActions(state,state.nextMovePlayerID), state);
		for (Action child : actions) {
			S clonedState = (S) state.deepCopy();
			logic.makeMove(clonedState, child);
			double value = MinMove(clonedState, alpha, beta, depth +1);

			if (bestValue < value) {
				bestValue = value;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(alpha >=beta){
				break;
			}
		}

		return bestValue;
	}

	private double MinMove(S state,double alpha,double beta, int depth) {

		if (logic.isTerminal(state) || this.maxSearchDepth == depth) {
			return eval.getScore(state)[currentPlayerID];
		}

		double bestValue = Double.POSITIVE_INFINITY;

		List<Action> actions = orderMoves(logic.generateActions(state,state.nextMovePlayerID), state);
		for (Action child : actions) {
			S clonedState = (S) state.deepCopy();
			logic.makeMove(clonedState, child);
			double value = MaxMove(clonedState, alpha, beta, depth +1);

			if (bestValue > value) {
				bestValue = value;
			}
			
			beta = Math.min(beta, bestValue);
			if(beta <=alpha){
				break;
			}
		}

		return bestValue;
	}
	
	protected List<Action> orderMoves(List<Action> actions, S state) {
		/*
		 * default move ordering is random
		 */
		return actions;
	}

}
