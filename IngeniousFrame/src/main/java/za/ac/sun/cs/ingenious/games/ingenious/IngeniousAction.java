package za.ac.sun.cs.ingenious.games.ingenious;

import za.ac.sun.cs.ingenious.core.model.Coord;
import za.ac.sun.cs.ingenious.core.model.Action;

public class IngeniousAction implements Action {

	private final Tile tile;
	private final Coord position;
	
	public IngeniousAction(Tile tile, Coord position){
		this.tile = tile;
		this.position = position;
	}
	
	public Tile getTile(){
		return this.tile;
	}
	public Coord getPosition(){
		return this.position;
	}
	public String toString(){
		String ret = "Tile : "+tile.toString()+"\n";
		ret += "Coordinate : "+ position.toString();
		return ret;
	}

	
}