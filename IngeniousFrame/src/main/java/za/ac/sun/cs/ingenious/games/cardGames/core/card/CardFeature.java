package za.ac.sun.cs.ingenious.games.cardGames.core.card;

/**
 * The Interface CardFeature.
 */
public interface CardFeature{
	
	/**
	 * Gets the value.
	 *
	 * @return value
	 */
	public int getValue();
	
	/**
	 * Sets the value.
	 *
	 * @param value The new value of the feature.
	 */
	public void setValue(int value);
}
