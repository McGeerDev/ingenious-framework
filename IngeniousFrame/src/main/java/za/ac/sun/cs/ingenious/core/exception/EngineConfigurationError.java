package za.ac.sun.cs.ingenious.core.exception;

import za.ac.sun.cs.ingenious.core.configuration.EngineConfig;

/**
 * Created by Chris Coetzee on 2016/07/18.
 */
public class EngineConfigurationError extends Exception {

    private EngineConfig engine = null;

    EngineConfigurationError() {}

    public EngineConfigurationError(String msg) {
        super(msg);
    }

    void setEngine(EngineConfig e) {
        this.engine = e;
    }

    public EngineConfig getEngine() {
        return engine;
    }
}
