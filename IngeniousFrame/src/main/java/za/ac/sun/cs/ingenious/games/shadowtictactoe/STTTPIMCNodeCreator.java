package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import za.ac.sun.cs.ingenious.search.mcts.MCTSNode;
import za.ac.sun.cs.ingenious.search.pimc.NodeCreator;

class STTTPIMCNodeCreator implements NodeCreator<STTTGameState, MCTSNode<STTTGameState>> {

	private static final STTTLogic logic = new STTTLogic();
	
	@Override
	public MCTSNode<STTTGameState> createNode(STTTGameState withState) {
		return new MCTSNode<STTTGameState>(withState, logic, null, null);
	}

}
