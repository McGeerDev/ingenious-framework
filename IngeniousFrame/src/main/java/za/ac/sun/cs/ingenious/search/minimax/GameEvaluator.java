package za.ac.sun.cs.ingenious.search.minimax;

import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.model.GameState;

/**
 * Used to evaluate the state of a game that has not yet ended.
 * Every game running MiniMax search most supply a class implementing this.
 * The methods are the same as in GameFinalEvaluator
 * @author Michael Krause
 */
public interface GameEvaluator<S extends GameState> extends GameFinalEvaluator<S> {

}
