# CardMoves

######Most typical moves you need for card games.

### CardMove (abstract)

The abstract class PlayCardMove is a superclass for different card moves (see below).

#### PlayCardMove

A PlayCardMove is sent when a player actively moves a certain card from an old location (e. g. him self) to a new location (e. g. the discard pile).


#### DrawCardMove

A DrawCardMove is sent when a player wants to (or is forced to) draw a card.

**Please note:** PlayCardMove and DrawCardMove are very similar. But for a game logic it might come in very handy to distinguish between the two move types. 