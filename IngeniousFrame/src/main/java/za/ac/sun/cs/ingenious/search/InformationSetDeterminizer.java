package za.ac.sun.cs.ingenious.search;

import za.ac.sun.cs.ingenious.core.model.GameState;

/**
 * A class implementing this interface can produce a determinization of information sets
 * represented by some GameState
 * @author Michael Krause
 * @param <S> GameState this Determinizer works with
 */
public interface InformationSetDeterminizer<S extends GameState> {
	/**
	 * @param forPlayerID The player whos view the information set given in forState represents
	 * @param forState The GameState (i.e. information set) to find a determinization for
	 * @return A determinization for forState
	 */
	public S determinizeUnknownInformation(int forPlayerID, S forState);
}
