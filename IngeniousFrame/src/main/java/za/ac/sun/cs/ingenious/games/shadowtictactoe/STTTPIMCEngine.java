package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Constants;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.core.model.XYAction;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTFinalEvaluator;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTSUCTDescender;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTS;
import za.ac.sun.cs.ingenious.search.mcts.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.UCTDescender;
import za.ac.sun.cs.ingenious.search.pimc.PIMC;

public class STTTPIMCEngine extends STTTEngine {
	
	public STTTPIMCEngine(EngineToServerConnection toServer) {
		super(toServer);
	}
	
	/**
	 * Never use spaces in engine name!
	 */
	@Override
	public String engineName() {
		return "STTTPIMCEngine";		
	}
	
	/**
	 * This method is called when the server requests a move by this engine. 
	 * Don't forget to update your own gamestate(board) if you make a move, receivePlayMove is only called for other players.
	 */
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		// This engine chooses a move with MCTS
		STTTGameState clonedState = (STTTGameState) currentState.deepCopy();
		return new PlayActionMessage((XYAction)PIMC.generateMove(new STTTPIMCNodeCreator(),
				playerID, logic, clonedState,
				new RandomPolicy<STTTGameState,MCTSNode<STTTGameState>>(logic, new STTTFinalEvaluator(), false),
				new UCTDescender<MCTSNode<STTTGameState>>(), 
				new SimpleUpdater<MCTSNode<STTTGameState>>(), 
				Constants.TURN_LENGTH, 80));
	}

}
