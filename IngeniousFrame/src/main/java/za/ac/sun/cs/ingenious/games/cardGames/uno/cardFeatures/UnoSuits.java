package za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;

//Values for suits don't play a role in Uno.

public enum UnoSuits implements CardFeature{
	RED(4),
	BLUE(3),
	GREEN(2),
	YELLOW(1);

	int value;
	
	UnoSuits(int value){
		this.value = value;
	}
	@Override
	public int getValue() {
		return this.value;
	}

	@Override
	public void setValue(int value) {
		this.value = value;
	}
}
