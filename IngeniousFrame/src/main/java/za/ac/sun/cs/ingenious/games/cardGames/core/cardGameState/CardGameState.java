package za.ac.sun.cs.ingenious.games.cardGames.core.cardGameState;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.Deck;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;

/**
 * The class CardGameState.
 * 
 * Represents a card game state with a TreeMap with Card&lt;F1,F2&gt; as key and ArrayList&lt;Locations&gt; as according value.
 *
 * @param <F1> Generic type of first feature (implements CardFeature).
 * @param <F2> Generic type of second feature (implements CardFeature).
 * @param <LocationEnum> Enum for all possible locations of a game (e. g. players or roles, draw piles, discard piles, ect...).
 */
public abstract class CardGameState
<F1 extends CardFeature, F2 extends CardFeature, LocationEnum extends Enum<LocationEnum>> 
extends TurnBasedGameState{

	/** The deck. */
	protected Deck<F1, F2> deck;
	
	/** The TreeMap. */
	protected TreeMap<Card<F1, F2>, ArrayList<LocationEnum>> map;

	/**
	 * Instantiates a new card game state.
	 *
	 * @param deck The card deck which is supposed to be transformed in a TreeMap.
	 * @param initLocationForAllCards The initial location for all cards.
	 * @param firstPlayerID The first player ID
	 */
	public CardGameState(Deck<F1, F2> deck, LocationEnum initLocationForAllCards, int firstPlayerID, int numPlayers) {
		super(firstPlayerID, numPlayers);
		this.deck = deck;
		map = new TreeMap<>();
		initCardLocationMap(initLocationForAllCards);
	}

	/**
	 * Initializes the card-location-map.
	 *
	 * @param initialLocationForAllCards is a single initial location where all cards will be at the beginning of a game (e. g. draw pile)
	 */
	public void initCardLocationMap(LocationEnum initialLocationForAllCards) {
		for (Card<F1, F2> card : this.deck.getAllCards()) {
			// By calling addCardType() only new card types get a new entry.
			this.addCardType(card);
			try {
				// In the ArrayList of locations another initial card location will be added.
				this.addLocation(card, initialLocationForAllCards);
			} catch (KeyNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Adds the card type if it doesn't exist already.
	 *
	 * @param newCardType the potential new card type
	 */
	protected void addCardType(Card<F1, F2> newCardType) {
		if (!map.containsKey(newCardType)) {
			map.put(newCardType, new ArrayList<LocationEnum>());
		}
	}

	/**
	 * Adds a location if the Card exists as a key in the TreeMap.
	 *
	 * @param card Is the key for the TreeMap.
	 * @param newLocation The new location.
	 * @throws KeyNotFoundException Raised if the key (Card) could not be found.
	 */
	public void addLocation(Card<F1, F2> card, LocationEnum newLocation) throws KeyNotFoundException {
		if (map.containsKey(card)) {
			map.get(card).add(newLocation);
		} else {
			throw new KeyNotFoundException("Invalid key! This deck doesn't contain " + card.toString() + ". ");
		}
	}

	/**
	 * Change location.
	 *
	 * @param cardType The card type.
	 * @param oldLocation The old location of the card type.
	 * @param newLocation The new location of the card type.
	 * @throws KeyNotFoundException Is raised if no such card type could be found as a key in the TreeMap.
	 * @throws LocationNotFoundException Is raised if no such old location for a card type could be found.
	 */
	public void changeLocation(Card<F1, F2> cardType, LocationEnum oldLocation, LocationEnum newLocation)
			throws KeyNotFoundException, LocationNotFoundException {

		// Check if cardType exists as a key in TreeMap.
		if (!map.containsKey(cardType)) {
			throw new KeyNotFoundException("Invalid key! This deck doesn't contain " + cardType.toString() + ". ");
		}

		// Get the ArrayList<Location> for a certain card type.
		ArrayList<LocationEnum> listOfLoc = map.get(cardType);

		// Iterate through ArrayList and if the old location exists change it to the new location.
		int i = 0;
		boolean locationExists = false;
		for (LocationEnum currLoc : listOfLoc) {
			if (currLoc.equals(oldLocation)) {
				// Change location
				listOfLoc.set(i, newLocation);
				// Keep in mind that old location exists
				locationExists = true;
				break;
			}
			i++;
		}

		// If old location could not be found raise LocationNotFoundException.
		if (!locationExists) {
			throw new LocationNotFoundException(
					"Apparently the location " + oldLocation + " does not exist for " + cardType.toString());
		}
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.model.GameState#printPretty()
	 * 
	 * Print the game state (TreeMap) pretty.
	 * 
	 * Print each card type and list all locations of this card type vertically and indented under it.
	 */
	@Override
	public void printPretty() {
		
		// Using a StringBuilder for Log.info().
		StringBuilder s = new StringBuilder();
		s.append("\n");
		
		// For each card type.
		for (Map.Entry<Card<F1, F2>, ArrayList<LocationEnum>> entry : map.entrySet()) {
			s.append(entry.getKey().toString() + ": \n");
			
			// For each location of the card type.
			for (LocationEnum location : entry.getValue()) {
				s.append("    " + location + "\n");
			}
			s.append("\n");
		}
		Log.info(s);
	}
	
	/**
	 * Gets the map.
	 *
	 * @return The map
	 */
	public TreeMap<Card<F1, F2>, ArrayList<LocationEnum>> getMap() {
		// consider a deep copy with Cloner (see super class GameState). But this would be very slow.
		return (TreeMap<Card<F1, F2>, ArrayList<LocationEnum>>) map.clone();
	}
	
	/**
	 * Sets the next player.
	 *
	 * @param nextPlayer The new next player
	 */
	public void setNextPlayer(int nextPlayer){
		super.nextMovePlayerID = nextPlayer;
	}
	
	/**
	 * Gets the current player.
	 *
	 * @return The current player
	 */
	public int getCurrentPlayer() {
		return super.nextMovePlayerID;
	}
}
