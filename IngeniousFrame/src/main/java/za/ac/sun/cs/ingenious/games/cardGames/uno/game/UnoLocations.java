package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

public enum UnoLocations {
	PLAYER0,
	PLAYER1,
	PLAYER2,
	PLAYER3,
	PLAYER4,
	PLAYER5,
	PLAYER6,
	PLAYER7,
	
	DRAWPILE,
	DISCARDPILE;
}