package za.ac.sun.cs.ingenious.games.mnk;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;

public class MNKBoard extends TurnBasedGameState {
	
	private int[][] board;
	private int height;
	private int width;
	private int k;
	private short winner;
	
	public int[][] getBoard() {
		return board;
	}
	
	public MNKBoard(int height, int width, int k, int firstPlayer){
		super(firstPlayer,2);
		this.height = height;
		this.width = width;
		this.k = k;
		this.board = new int[height][width];
		this.winner = -1; // No winner yet
	}
	
	//copy constructor
	public MNKBoard(MNKBoard board){
		super(board.nextMovePlayerID, board.numPlayers);
		this.height = board.getHeight();
		this.width = board.getWidth();
		this.k = board.getK();
		this.winner = board.winner;
		
		this.board = new int[board.getHeight()][board.getWidth()];
		for(int h=0; h<this.height; h++){
			for(int c=0; c<this.width; c++){
				this.board[h][c] = board.getBoard()[h][c];
			}
		}
		
	}
	
	public MNKBoard(int[][] board, int k, int firstPlayer){
		super(firstPlayer,2);
		this.board = board;
		this.height = board.length;
		this.width = board[0].length;
		this.k = k;
	}
	
	@Override
	public void printPretty(){
		StringBuilder s = new StringBuilder();
		s.append("\n+");
		for(int i=0; i<this.width;i++){
			s.append("-");
		}
		s.append("+\n");
		
		for(int i=0; i<this.height;i++){
			s.append("|");
			for(int j=0; j<this.width;j++){
				s.append(this.board[i][j]);
			}
			s.append("|\n");
		}
		
		s.append("+");
		for(int i=0; i<this.width;i++){
			s.append("-");
		}
		s.append("+ \n");
		Log.info(s);
		
	}
	
	/** Store the winner in the state, since detecting terminal states involves determining the winner. */
	protected void setWinner(short w) {
		this.winner = w;
	}
	
	protected short getWinner() {
		return this.winner;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	public int getWidth(){
		return this.width;
	}
	
	public int getK(){
		return this.k;
	}
	
}
