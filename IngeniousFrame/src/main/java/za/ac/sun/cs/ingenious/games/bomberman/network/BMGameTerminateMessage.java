package za.ac.sun.cs.ingenious.games.bomberman.network;

import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;

public class BMGameTerminateMessage extends GameTerminatedMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double[] winner;
	
	public BMGameTerminateMessage(double[] winner) {
		this.winner = winner;
	}
	
	
	/**
	 * @return an array of doubles, one per player - the player with the highest value is the winner.
	 */
	public double[] getWinningScore() {
		return winner;
	}
	

}
