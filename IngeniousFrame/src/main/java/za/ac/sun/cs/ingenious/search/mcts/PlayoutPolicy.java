package za.ac.sun.cs.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.core.model.GameState;

/**
 * A playout policy describes how the simulations in MCTS are run once the best
 * node in the search tree has been found and expanded. A standard implementation is
 * {@link RandomPolicy}
 * @author steve
 */
public interface PlayoutPolicy<N extends SearchNode<? extends GameState,N>> {
	
	/**
	 * Performs a playout
	 * @param node The SearchNode from which to start the playout.
	 * @return Scores per player after the playout
	 */
	public double[] playout(N node);
	
}
