package za.ac.sun.cs.ingenious.games.cardGames.uno.engines;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.Deck;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.move.DrawCardMove;
import za.ac.sun.cs.ingenious.games.cardGames.core.move.PlayCardMove;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoGameLogic;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoGameState;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoInitGameMessage;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoLocations;

public class UnoRandomEngine extends Engine {

	UnoGameState state;
	UnoGameLogic logic;

	public UnoRandomEngine(EngineToServerConnection toServer) {
		super(toServer);
		logic = new UnoGameLogic();
	}

	@Override
	public String engineName() {
		return "UnoRandomPlayer";
	}

	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		UnoInitGameMessage uiga = (UnoInitGameMessage) a;
		CardRack<UnoSymbols, UnoSuits> rack = uiga.getRack();

		Deck<UnoSymbols, UnoSuits> deck = new Deck<>(EnumSet.allOf(UnoSymbols.class), EnumSet.allOf(UnoSuits.class), 2);
		state = new UnoGameState(deck, uiga.getNumPlayers());

		for (Card card : rack) {
			try {
				state.changeLocation(card, UnoLocations.DRAWPILE, UnoLocations.values()[super.playerID]);
			} catch (KeyNotFoundException e) {
				e.printStackTrace();
			} catch (LocationNotFoundException e) {
				e.printStackTrace();
			}
		}		
	}

	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a) {
		Move move = a.getMove();

		if (move instanceof DrawCardMove) {
			DrawCardMove dcMove = (DrawCardMove) move;
			try {
				state.changeLocation(dcMove.getCard(), UnoLocations.DRAWPILE, UnoLocations.values()[playerID]);
			} catch (KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}

		else if (move instanceof PlayCardMove) {
			PlayCardMove pcMove = (PlayCardMove) move;
			state.changeTop(pcMove.getCard());
			if (pcMove.getPlayer() == this.getPlayerID()) {
				try {
					state.changeLocation(pcMove.getCard(), UnoLocations.values()[pcMove.getPlayer()],
							(UnoLocations) pcMove.getNewLoc());
				} catch (KeyNotFoundException | LocationNotFoundException e) {
					e.printStackTrace();
				}
			} else {
				try {
					state.changeLocation(pcMove.getCard(), UnoLocations.DRAWPILE, (UnoLocations) pcMove.getNewLoc());
				} catch (KeyNotFoundException | LocationNotFoundException e) {
					e.printStackTrace();
				}

			}
		}
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		ArrayList<Action> actions = (ArrayList<Action>) logic.generateActions(state, this.playerID);
		if (actions.size() == 0) {
			return null;
		}
		
		Random rand = new Random();
		Action randomMove = actions.get(rand.nextInt(actions.size()));
		return new PlayActionMessage(randomMove);
	}

	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {

	}

}
