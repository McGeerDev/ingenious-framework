package za.ac.sun.cs.ingenious.core;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;

/**
 * Abstract super class for referees for any turn based game with full observable moves.
 */
public abstract class FullyObservableMovesReferee<S extends TurnBasedGameState, L extends TurnBasedGameLogic<S>, E extends GameFinalEvaluator<S>> extends TurnBasedReferee<S, L, E> {

	protected FullyObservableMovesReferee(MatchSetting match, PlayerRepresentation[] players, S currentState, L logic,
			E eval) {
		super(match, players, currentState, logic, eval);
	}

}
