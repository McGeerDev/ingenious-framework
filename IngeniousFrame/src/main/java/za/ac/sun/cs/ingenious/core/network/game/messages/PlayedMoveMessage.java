package za.ac.sun.cs.ingenious.core.network.game.messages;

import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * Sent by the referee to a player to indicate that the contained move has been made
 */
public class PlayedMoveMessage extends Message {

	private static final long serialVersionUID = 1L;

	private Move m;

	public PlayedMoveMessage(Move m) {
		this.m = m;
	}

	/**
	 * @param a A PlayActionMessage, whose action is taken to construct this message from.
	 */
	public PlayedMoveMessage(PlayActionMessage a) {
		this(a.getAction());
	}

	public Move getMove() {
		return m;
	}

}
