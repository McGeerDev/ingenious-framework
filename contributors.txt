Marc Christoph - contributions to Lines of Action and integration with Distributed Tournament Engine
Chris Coetzee - contributions to networking code, development of command-line scripts, and integration with Distributed Tournament Engine
Michael Krause - intern on project 2016 (general framework aspects, Domineering, shadow Tic-Tac-Toe, ISMCTS)
Steve Kroon - project co-ordinator
Steven Labrum - original Ingenious implementation for 2016 Stellenbosch University Computer Science Honours project
Stephan Tietz - intern on project 2016 (general framework aspects, Bomberman, Carcassonne)
Joshua Wiebe - intern on project 2016 (general framework aspects, mnk-game, card games)
